const express = require('express');
const router = express.Router()
const OrderController = require('../controllers/OrderController');
const auth = require('../auth.js');


// checkout order
router.post('/checkout', auth.verify, OrderController.placeOrder); 

// get all orders
router.get('/all', auth.verify, auth.verifyAdmin, OrderController.getAllOrders);
module.exports = router;