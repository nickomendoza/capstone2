const express = require('express');
const router = express.Router()
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');
const Product = require('../models/Product.js');

// Add a new product

router.post('/add-new-product', auth.verify, auth.verifyAdmin , ProductController.addNewProduct);

// Get all listed products

router.get('/all', ProductController.getAllProducts)

// See available/inStock products

router.get('/', (request, response) => {
  ProductController.getInStock(request, response);
})

// See specific product
router.get('/:id', (request, response) => {
	ProductController.getProduct(request, response);
})

// Update product information
router.put('/:id/edit-info', auth.verify, auth.verifyAdmin, (request, response) => {
  ProductController.updateProduct(request, response);
})

// Archive Specific product
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
  ProductController.archiveProduct(request, response);
})

// Activate Specific product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
  ProductController.activateProduct(request, response);
})

// Search product by name
router.post('/search', (request, response) =>{
	ProductController.searchProduct(request, response);
});

module.exports = router;