const express = require('express');
const router = express.Router()
const CartController = require('../controllers/CartController.js');
const auth = require('../auth.js');

// Add item to the cart
router.post('/add-to-cart', auth.verify, CartController.addToCart);

// Get cart
router.get('/', auth.verify, CartController.getCart);

// clear cart 
router.delete('/clear', auth.verify, CartController.clearCart);

// remove specific item
router.delete('/item/:productId', auth.verify, CartController.removeCartItem);
module.exports = router;