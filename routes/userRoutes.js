const express = require('express');
const router = express.Router()
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// User Registration
router.post('/register', (request, response) => {
  UserController.registerUser(request.body).then((result) => {
    response.send(result);
  })
})

// User Login
router.post('/login', (request, response) => {
  UserController.loginUser(request, response)
})

// User Profile
router.get("/details", auth.verify, UserController.getProfile)


// User orders
router.get('/my-orders', auth.verify, (request, response) => {
  UserController.getUserOrders(request, response);
})

// Set specific user as Admin
router.put('/:id/set-as-admin', auth.verify, auth.verifyAdmin, (request, response) => {
  UserController.setUserAsAdmin(request, response);
})

// Archive specific admin
router.put('/:id/archive-admin', auth.verify, auth.verifyAdmin, (request, response) => {
  UserController.archiveAdmin(request, response);
})
// Update user profile
router.put('/profile', auth.verify, UserController.updateProfile);


module.exports = router;