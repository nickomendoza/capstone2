const mongoose = require('mongoose');

const product_schema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Product name is required.']
  },
  description: {
    type: String,
    required: [true, 'Product description is required.']
  },
  price: {
    type: Number,
    required: [true, 'Product price is required.']
  },
  category: {
    type: String,
    required: [true, 'Product price is required.']
  },
  imgUrl:{
    type: String,
    required: [true, 'Product price is required.']
  },
  inStock: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: new Date()
  }
})

module.exports = mongoose.model('Product', product_schema);