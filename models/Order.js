const mongoose = require('mongoose');

const order_schema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'userId is required.']

  },
  products: [{
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    }
  }],
  totalAmount: {
    type: Number,
    required: [true, "total amount is required"]
  },
  dateTransaction: {
    type: Date,
    default: new Date()
  }
})

module.exports = mongoose.model('Order', order_schema);