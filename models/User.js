const mongoose = require('mongoose');

const user_schema = new mongoose.Schema({
  firstName : {
    type: String,
    required: [true, 'Please input your First Name']
  },
  lastName : {
    type: String,
    required: [true, 'Please input your Last Name']
  },
  email : {
    type: String,
    required: [true, 'Email is required.']
  },
  mobileNum : {
    type: String,
    required: [true, 'Please input your mobile number.']
  },
  password : {
    type: String,
    required : [true, 'Password is required.']
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('User', user_schema);