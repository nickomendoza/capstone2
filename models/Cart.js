const mongoose = require('mongoose');

const cart_schema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, 'UserId is required.']
  },
  addedProducts: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Product', 
        required: true 
      },
      quantity: {
        type: Number,
        required: [true, 'quantity is required']
      }

    }

  ],
  totalCartAmount: {
    type: Number,
    required: [true, 'totalCartAmount is required']
  },
  dateTransaction: {
    type: Date,
    default: new Date()
  }
})
module.exports = mongoose.model('Cart', cart_schema);