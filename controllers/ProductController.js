const Product = require('../models/Product.js');

// Adding a Product (Admin only)
module.exports.addNewProduct = (request, response) => {
  let new_product = new Product({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    category: request.body.category,
    imgUrl: request.body.imgUrl
  });
  
  return new_product.save().then((saved_product, error) => {
		if (error){
			return response.send(false);
		}

		return response.send(true);


	}).catch(error => console.log(error));

}

// Get all listed products
module.exports.getAllProducts = (request, response) => {
  return Product.find({}).then(listed_products => {
    return response.send(listed_products);
  })
}

// get all in stock products

module.exports.getInStock = (request, response) => {
  return Product.find({inStock: true}).then(result => {
    return response.send(result);
  })
}

// See specific product
module.exports.getProduct = (request, response) => {
  return Product.findById(request.params.id).then(result => {
    return response.send(result);
  })
}

// Update product information
module.exports.updateProduct = (request, response) => {
	let updated_product_info = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
    category: request.body.category,
    imgUrl: request.body.imgUrl
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_info).then((product, error) => {
		if (error){
			return response.send(false);
		}

		return response.send(true);
	})
}

// Archiving specific product
module.exports.archiveProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, { inStock: false })
  .then((result, error) => {
    if (error) {
      return response.send(false)
    }
    return response.send(true)
  })
}

// Activate specific product
module.exports.activateProduct = (request, response) => {
  return Product.findByIdAndUpdate(request.params.id, { inStock: true })
  .then((result, error) => {
    if (error) {
      return response.send(false)
    }
    return response.send(true)
  })
}

// Search product by name
module.exports.searchProduct = (request, response) => {
  return Product.find({name: {$regex: request.body.name, $options: 'i'} })
  .then((product) => {
    response.send(product);
  }).catch(error => response.send({message: error.message}))
}
