const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Order = require('../models/Order.js');

// User Registration
module.exports.registerUser = (request_body) => {

  let new_user = new User({
    firstName: request_body.firstName,
    lastName: request_body.lastName,
    email: request_body.email,
    mobileNum: request_body.mobileNum,
    password: bcrypt.hashSync(request_body.password, 10)
  });
  return new_user.save().then((user, error) => {
    if(error){
      return false;
    }
    return true;
  }).catch(err=>response.send(err))
};

// User Login
module.exports.loginUser = (request, response) => {
  return User.findOne({ email: request.body.email })
    .then((result) => {
      if (result == null) {
        return response.send(false);
      }
      const is_password_correct = bcrypt.compareSync(
        request.body.password,
        result.password
      );

      if (is_password_correct) {
        return response.send({ access: auth.createAccessToken(result) });
      } else {
        return response.send(false);
      }
    })
    .catch((error) => response.send(error));
};

// User Profile
module.exports.getProfile = (req, res) => {


  return User.findById(req.user.id).then(result => {

    result.password = "";
    return res.send(result);

  })
  .catch(err => res.send(err))
};

// see User orders
module.exports.getUserOrders = (request, response) => {
  return Order.find({userId: request.user.id}).populate('products.productId').then(result => {
    return response.send(result);
  })
}



// Set user as admin
module.exports.setUserAsAdmin = (request, response) => {
  return User.findByIdAndUpdate(request.params.id, { isAdmin: true })
  .then((result, error) => {
    if (error) {
      return response.send(false);
    }
    return response.send(true);
  })
}

// Archive specific Admin
module.exports.archiveAdmin = (request, response) => {
  return User.findByIdAndUpdate(request.params.id, { isAdmin: false })
  .then((result, error) => {
    if (error) {
      return response.send(false);
    }
    return response.send(true);
  })
}

// for updating profile

module.exports.updateProfile = (req, res) => {
  const userId = req.user.id;

  const { firstName, lastName, email, mobileNum } = req.body;

  const updatedUser = User.findByIdAndUpdate(userId, {firstName, lastName, email, mobileNum}, {new:true})
  .then((result, error) => {
    if(error) {
      return response.send(false);
    }

    return res.json(true);
  })

}