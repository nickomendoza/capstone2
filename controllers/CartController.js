const Cart = require('../models/Cart.js');
const Product = require('../models/Product.js');

module.exports.addToCart = async (req, res) => {
	const { productId, quantity } = req.body;
	const userId = req.user.id;

	try {
		let cart = await Cart.findOne({userId});

		if (!cart){
			cart = new Cart({
				userId,
				addedProducts: [],
				totalCartAmount: 0
			});
		}

		const existingProduct = cart.addedProducts.find(product => product.productId.toString() === productId);

		if (existingProduct) {
			existingProduct.quantity+= quantity;
		} else {
			cart.addedProducts.push({productId, quantity});
		}

		let total = 0;
	    for (let product of cart.addedProducts) {
	      const productData = await Product.findById(product.productId);
	      total += productData.price * product.quantity;
	    }

	    cart.totalCartAmount = total;

		await cart.save();

		res.json(cart);
	} catch (error) {
    console.error('Error adding to cart:', error);
    res.status(500).json({ message: 'Error adding to cart' });
  }
};

module.exports.getCart = async (req, res) => {
	const userId = req.user.id;

	try {
		const cart = await Cart.findOne({userId}).populate('addedProducts.productId');

		if(!cart) {
			return res.json(error);
		}

		res.json(cart);
	} catch (error) {
    console.error('Error fetching cart:', error);
    res.status(500).json({ message: 'Error fetching cart' });
  }
}

module.exports.clearCart = async (req, res) => {
  const userId = req.user.id;

  try {
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      return res.status(404).json({ message: 'Cart not found' });
    }

    // Clear the cart
    cart.addedProducts = [];
    cart.totalCartAmount = 0;

    await cart.save();

    res.json(true);
  } catch (error) {
    console.error('Error clearing cart:', error);
    res.status(500).json({ message: 'Error clearing cart' });
  }
};

module.exports.removeCartItem = async (req, res) => {
	const userId = req.user.id;
  const { productId } = req.params

  try {
  	const cart = await Cart.findOne({ userId });

    if (!cart) {
      return res.status(404).json({ message: 'Cart not found' });
    }

    // Find the product in the cart
    const productIndex = cart.addedProducts.findIndex(p => p.productId.toString() === productId);

    if (productIndex === -1) {
      return res.status(404).json({ message: 'Product not found in cart' });
    }

    const productToRemove = cart.addedProducts[productIndex];
    const productData = await Product.findById(productId);
    
    cart.totalCartAmount -= productData.price * productToRemove.quantity;

     cart.addedProducts.splice(productIndex, 1);

    await cart.save();

    res.json(true);
    
  } catch (error) {
    console.error('Error deleting cart item:', error);
    res.status(500).json({ message: 'Error deleting cart item' });
  }
}