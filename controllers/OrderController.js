const Cart = require('../models/Cart');
const Order = require('../models/Order');
const Product = require('../models/Product');


module.exports.placeOrder = async (req, res) => {
  try{
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart || cart.addedProducts.length === 0) {
        return res.status(400).json(false);
    }

    const orderProducts = await Promise.all(cart.addedProducts.map(async product => {
        const productData = await Product.findById(product.productId);
        return {
            productId: product.productId,
            productName: productData.name,
            price: productData.price,
            quantity: product.quantity
        };
    }));

    const order = new Order({
        userId: req.user.id,
        products: orderProducts,
        totalAmount: cart.totalCartAmount
    });

    await order.save();

    cart.addedProducts = [];

    cart.totalCartAmount = 0;

    await cart.save();

    res.send(order);
  } catch (error) {
    console.error('Error placing order:', error);
    res.status(500).json({ message: 'Error placing order' });
  }

}

module.exports.getAllOrders = async (req, res) => {
  try {
    const allOrders = await Order.find().populate('products.productId');
    res.json(allOrders);
  } catch (error) {
    console.error('Error fetching all orders:', error);
    res.status(500).json({ message: 'Error fetching all orders' });
  }
}
